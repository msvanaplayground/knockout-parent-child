# Component communication example

Simple example of communication between parent and child components in KnockoutJS.
Parent passes the child a function which is part of it's own viewModel as a parameter 
and child calls this function when user clicks a button inside the child.
